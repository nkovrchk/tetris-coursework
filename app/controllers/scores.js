import db from './../../server';

export let GetScorePage = async (req, res)=>{
    try{
        let scores = await db.any('select * from tetris order by score',[true]);
        res.render('scores.html',{scores:scores.reverse()});
    }catch(error){
        console.log(error);
    }
} 