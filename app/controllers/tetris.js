import db from './../../server';

export let GetTetrisPage = (req,res)=>{
    res.render('tetris.html');
}

export let AddScore = async (req,res)=>{
    let item;
    try{
        item = await db.one('select * from tetris WHERE name = $1', [req.body.name]);
        if(parseInt(item.score) < parseInt(req.body.score)){
            db.none('update tetris set score = $1 where name = $2', [req.body.score, item.name]);
            res.status(200).send('Данные успешно обновлены');
        }else{
            res.status(200).send('Вы уже имеете больший счет');
        }
    }catch(error){
        db.none('insert into tetris (name, score) values ($1, $2)', [req.body.name, req.body.score]);
        res.status(200).send('Данные успешно записаны');
    }
}