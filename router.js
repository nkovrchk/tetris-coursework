//импорт Express и контроллеров
import express from 'express';
import * as main_controller from './app/controllers/main';
import * as tetris_controller from './app/controllers/tetris';
import * as score_controller from './app/controllers/scores';
import * as howto_controller from './app/controllers/howto';
import * as about_controller from './app/controllers/about';

//инициализация роутера
let router = express.Router();

//Main
router.get('/', main_controller.GetMainPage);

//Tetris
router.get('/tetris', tetris_controller.GetTetrisPage);
router.post('/api/sendScore', tetris_controller.AddScore);

//Scores
router.get('/scores', score_controller.GetScorePage);

//How to
router.get('/howto', howto_controller.RenderHowToPage);

//About
router.get('/about', about_controller.RenderAboutPage);

export default router;