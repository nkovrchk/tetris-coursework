//импорт модулей
import express from 'express';
import nunjucks from 'nunjucks';
import bodyParser from 'body-parser';
import router from './router';
import config from './config';
let pgp = require('pg-promise')();

//инициализация веб-приложения, базы данных
const app = express(),
    db = pgp({
        host: config.dbHost,
        port: config.dbPort,
        database: config.dbName,
        user: config.dbUser,
        password: config.dbPassword
    });

//конфигурация шаблонного движка
nunjucks.configure(__dirname+ '/src/html', {
    autoescape: true,
    express: app
});

//конфигурация статических файлов для корректного доступа серверу
app.use(bodyParser());
app.use('/',express.static(__dirname + '/src'));
app.use('/js',express.static(__dirname + '/node_modules/bootstrap/dist/js'),express.static(__dirname + '/node_modules/popper.js/dist'),express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/css',express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use(router);

//прослушивание порта для получения запросов
app.listen(config.port, ()=>{
    console.log('Сервер прослушивает подключения на порте: ' + config.port);
});

//экспорт бд для использования в контроллерах
export default db;