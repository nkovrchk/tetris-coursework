const COLORS = {
    'E': "#ffffff",
    'B': "#007bff",
    'O': "#fd7e14",
    'P': "#6f42c1",
    'R': "#dc3545",
    'Y': "#ffc107",
    'C': "#17a2b8",
    'G': "#28a745"
};

const KEYS = { 
    ESC: 27, 
    SPACE: 32, 
    LEFT: 37, 
    UP: 38, 
    RIGHT: 39, 
    DOWN: 40 
};