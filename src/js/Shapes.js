class Shape {
    constructor() {
      this.orientation = "u";
      this.matrix = [];
      this.x = 3;
      this.y = 1;
    }
    rotate() {
      const n = this.matrix.length;
      let newArray = Array(n)
        .fill()
        .map(() => []);
      for (let y = 0; y < n; y++) {
        for (let x = 0; x < n; x++) {
          newArray[y][x] = this.matrix[n - x - 1][y];
        }
      }
      this.matrix = newArray;
      newArray = [];
      this.updateOrientation();
    }
    updateOrientation() {
      const orientationArray = ["u", "r", "d", "l"];
      const oldOrientation = this.orientation;
  
      const newOrientIndex = (orientationArray.indexOf(oldOrientation) + 1) % 4;
      this.orientation = orientationArray[newOrientIndex];
    }
    moveRight() {
      this.x += 1;
    }
    moveLeft() {
      this.x -= 1;
    }
    moveDown() {
      this.y += 1;
    }
  }
  class ShapeL extends Shape {
    constructor() {
      super();
      this.color = "B";
      this.matrix = [["B", "E", "E"], ["B", "B", "B"], ["E", "E", "E"]];
    }
  }
  
  class ShapeJ extends Shape {
    constructor() {
      super();
      this.color = "J";
      this.matrix = [["E", "E", "O"], ["O", "O", "O"], ["E", "E", "E"]];
    }
  }
  
  class ShapeZ extends Shape {
    constructor() {
      super();
      this.color = "R";
      this.matrix = [["R", "R", "E"], ["E", "R", "R"], ["E", "E", "E"]];
    }
  }
  
  class ShapeS extends Shape {
    constructor() {
      super();
      this.color = "G";
      this.matrix = [["E", "G", "G"], ["G", "G", "E"], ["E", "E", "E"]];
    }
  }
  
  class ShapeI extends Shape {
    constructor() {
      super();
      this.color = "C";
      this.matrix = [
        ["E", "E", "E", "E"],
        ["C", "C", "C", "C"],
        ["E", "E", "E", "E"],
        ["E", "E", "E", "E"]
      ];
    }
  }
  
  class ShapeO extends Shape {
    constructor() {
      super();
      this.color = "Y";
      this.matrix = [["Y", "Y"], ["Y", "Y"]];
    }
  
    rotate() {
      // do nothing
    }
  }
  
  class ShapeT extends Shape {
    constructor() {
      super();
      this.color = "P";
      this.matrix = [["P", "P", "P"], ["E", "P", "E"], ["E", "E", "E"]];
    }
  }
