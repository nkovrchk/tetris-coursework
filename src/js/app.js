$('.input-score').bind('keyup', ()=>{CheckNameInput();});

function CheckNameInput(){
    $('.send-name').attr('disabled', $('.input-score').val().length < 6? true: false);
}

function PopupWindow(text, css_props){
    $('.popup-text').text(text);
    $('.popup-window').css(css_props).toggleClass('open close');
    setTimeout(()=>{
        $('.popup-window').toggleClass('open close');
    },3000)
}