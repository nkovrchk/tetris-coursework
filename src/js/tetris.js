class Tetris {
  constructor(canvas) {
    this.is_allowed_to_start = true;
    this.canvas = canvas;
    this.rows = 20;
    this.columns = 10;
    this.canvas.height = window.innerHeight - 10;
    this.canvas.width = this.canvas.height / 1.25;
    this.scaleFactor = this.canvas.height / this.rows;
    this.ctx = canvas.getContext("2d");
    this.speed = 1;
    this.score = 0;
    this.gameBoard = []; //make 10*20 game board
    this.currentTet = {};
    this.ResetGameBoard();
    document.addEventListener("keydown", ev => this.HandleKeyDown(ev), false);
    window.addEventListener("resize", ev => this.ResizeCanvas(ev), false);
  }

  ResetGameBoard() {
    this.is_allowed_to_start = true;
    this.gameBoard = [];
    for (let i = 0; i < this.rows; i++) {
      const rowsTemp = Array(this.columns).fill("E");
      this.gameBoard.push(rowsTemp);
    }
    this.score = 0;
    this.speed = 1;
    this.RenderGameBoard();
  }

  ResizeCanvas(ev) {
    this.canvas.height = window.innerHeight - 10;
    this.canvas.width = this.canvas.height / 1.25;
    this.scaleFactor = this.canvas.height / this.rows;
    this.RenderGameBoard();
  }

  DrawRectangle(x, y, color) {
    const scaledX = x * this.scaleFactor;
    const scaledY = y * this.scaleFactor;
    this.ctx.fillStyle = color;
    this.ctx.fillRect(scaledX, scaledY, this.scaleFactor, this.scaleFactor);
    this.ctx.strokeStyle = "white";
    this.ctx.lineWidth = 0.5;
    this.ctx.strokeRect(scaledX, scaledY, this.scaleFactor, this.scaleFactor);
  }

  DrawScore() {
    this.ctx.font = `${this.scaleFactor * 0.8}px Arial`;
    this.ctx.fillStyle = "#000";
    this.ctx.textAlign = "right";
    this.ctx.fillText(
      "Счет: " + this.score,
      this.scaleFactor * 9.3,
      this.scaleFactor * 1.3
    );
  }


  RenderGameBoard() {
    for (let y = 0; y < this.rows; y++) {
      for (let x = 0; x < this.columns; x++) {
        const colourOfBlock = COLORS[this.gameBoard[y][x]];
        this.DrawRectangle(x, y, colourOfBlock);
      }
    }
    this.DrawScore();
  }

  RedrawShape() {
    this.InsertShape();
    this.RenderGameBoard();
  }

  IsValidMove(piece) {
    const n = piece.matrix.length;
    for (let y = 0; y < n; y++) {
      for (let x = 0; x < n; x++) {
        const boardXOffset = piece.x + x;
        const boardYOffset = piece.y + y;
        if (piece.matrix[y][x] !== "E") {
          //try to get gameboard at board offsets, if exception, then piece is out of bounds
          try {
            const bounds = this.gameBoard[boardYOffset][boardXOffset];

            if (bounds !== "E" || boardXOffset === this.columns) {
              throw new Error("hit something");
            }
          } catch (e) {
            return false;
          }
        }
      }
    }
    return true;
  }

  ClearShape() {
    const n = this.currentTet.matrix.length;
    for (let y = 0; y < n; y++) {
      for (let x = 0; x < n; x++) {
        const boardXOffset = this.currentTet.x + x;
        const boardYOffset = this.currentTet.y + y;
        if (this.currentTet.matrix[y][x] !== "E")
          this.gameBoard[boardYOffset][boardXOffset] = "E";
      }
    }
  }

  InsertShape() {
    const n = this.currentTet.matrix.length;
    for (let y = 0; y < n; y++) {
      for (let x = 0; x < n; x++) {
        const boardXOffset = this.currentTet.x + x;
        const boardYOffset = this.currentTet.y + y;
        if (this.currentTet.matrix[y][x] !== "E")
          this.gameBoard[boardYOffset][boardXOffset] = this.currentTet.matrix[
            y
          ][x]; //inital check will make sure not out of bounds
      }
    }
  }

  HandleShapeRotation() {
    this.ClearShape();
    let copyPiece = new Shape();
    copyPiece.x = this.currentTet.x;
    copyPiece.y = this.currentTet.y;
    copyPiece.matrix = this.currentTet.matrix;
    copyPiece.orientation = this.currentTet.orientation;
    copyPiece.rotate();
    if (this.IsValidMove(copyPiece) && this.IsBottomEmpty(copyPiece)) {
      this.currentTet.rotate();
    }
    this.RedrawShape();
  }

  IsBottomEmpty(piece) {
    const n = piece.matrix.length;
    for (let y = 0; y < n; y++) {
      for (let x = 0; x < n; x++) {
        const boardXOffset = piece.x + x;
        const boardYOffsetBelow = piece.y + y + 1;

        if (piece.matrix[y][x] !== "E") {
          //try to get gameboard at board offsets, if exception, then piece is out of bounds
          try {
            const bounds = this.gameBoard[boardYOffsetBelow][boardXOffset];
            if (bounds !== "E") return false;
          } catch (e) {
            return false;
          }
        }
      }
    }
    return true;
  }

  CheckRows() {
    let gameBoardCopy = this.gameBoard;
    for (let row = this.rows - 1; row >= 0; row--) {
      let filteredRow = gameBoardCopy[row].filter(val => {
        return val === "E";
      });

      if (filteredRow.length === 0) {
        this.gameBoard.splice(row, 1);
        const rowsTemp = Array(this.columns).fill("E");
        this.gameBoard.unshift(rowsTemp);
        this.score += 100 * this.speed;
        row++;
      }
    }
    this.RenderGameBoard();
  }

  SpawnNewShape() {
    this.CheckRows();
    this.score += 10 * this.speed;
    const Shapes = [
      new ShapeI(),
      new ShapeJ(),
      new ShapeL(),
      new ShapeS(),
      new ShapeZ(),
      new ShapeO(),
      new ShapeT()
    ];

    let newTet = Shapes[Math.floor(Math.random() * Shapes.length)];
    if (!this.IsValidMove(newTet)) {
      this.is_allowed_to_start = false;
      this.ShowWindow();
      //this.ResetGameBoard();
    }
    this.currentTet = newTet;
    this.InsertShape();
  }

  MoveTo(direction) {
    if (this.is_allowed_to_start) {
      this.ClearShape();
      let copyPiece = new Shape();
      copyPiece.x = this.currentTet.x;
      copyPiece.y = this.currentTet.y;
      copyPiece.matrix = this.currentTet.matrix;

      switch (direction) {
        case KEYS.DOWN:
          copyPiece.moveDown();
          if (this.IsValidMove(copyPiece)) {
            this.currentTet.moveDown();
          }
          break;
        case KEYS.RIGHT:
          copyPiece.moveRight();
          if (this.IsValidMove(copyPiece)) {
            this.currentTet.moveRight();
          }
          break;
        case KEYS.LEFT:
          copyPiece.moveLeft();
          if (this.IsValidMove(copyPiece)) {
            this.currentTet.moveLeft();
          }
          break;
      }
      if (!this.IsBottomEmpty(this.currentTet)) {
        this.InsertShape();
        this.SpawnNewShape();
      } else {
        this.RedrawShape();
      }
    }
  }

  HandleKeyDown(ev) {
    switch (ev.keyCode) {
      case KEYS.UP:
        this.HandleShapeRotation();
        break;
      case KEYS.DOWN:
        this.MoveTo(KEYS.DOWN);
        break;
      case KEYS.RIGHT:
        this.MoveTo(KEYS.RIGHT);
        break;
      case KEYS.LEFT:
        this.MoveTo(KEYS.LEFT);
        break;
    }
  }

  Play() {
    let first_shapes = [new ShapeL(), new ShapeI(), new ShapeT(), new ShapeO(), new ShapeS(), new ShapeZ(), new ShapeJ()];
    this.currentTet = first_shapes[Math.floor(Math.random() * first_shapes.length)];
    this.InsertShape();

    this.RenderGameBoard();
    window.animateLoop = () => {
      setTimeout(() => {
        requestAnimationFrame(animateLoop);
        this.MoveTo(KEYS.DOWN);
        this.RenderGameBoard();

      }, 2000 / this.speed);
    };

    setInterval(() => {
      this.speed++;
    }, 30000);
    animateLoop();
  }
  
  ShowWindow() {
    $('.modal-fallback').addClass('show');
    $('.score-val').text(`Ваш счет: ${this.score}`);
  }

  SendScore() {
    $.ajax({
      method: 'POST',
      data: {
        name: $('.input-score').val(),
        score: this.score
      },
      url: '/api/sendScore',
      success: (data) => {
        PopupWindow(data, {
          'background-color': '#28a745'
        });
      }
    });
    $('.modal-fallback').removeClass('show');
    this.ResetGameBoard();
  }
}